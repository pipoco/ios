//
//  ViewController.swift
//  events
//
//  Created by Julio Effgen on 04/07/16.
//  Copyright © 2016 Julio Effgen. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class ViewController: UIViewController {
	
	func fetch() {
		let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
		let managedContext = appDelegate.managedObjectContext
		let personFetch = NSFetchRequest(entityName: "Link")
		
		do {
			let fetchedPerson = try managedContext.executeFetchRequest(personFetch) as! [Link]
			
			if (fetchedPerson.count > 0) {
				for person in fetchedPerson {
					if person.detail == "tela" {
						person.detail = "tela"
						person.link = "tubis";
						try person.managedObjectContext?.save()
						print(person.detail! + " " + person.link!)
					} else {
						managedContext.deleteObject(person)
						try person.managedObjectContext?.save()
					}
					
				}
			}
		} catch {
			fatalError("Failed to fetch person: \(error)")
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let date = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: -17, toDate: NSDate(), options: [])!
		let range = NSCalendar.currentCalendar().rangeOfUnit(.Day, inUnit: .Month, forDate: date)
		print(range.length)
  		fetch()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}

