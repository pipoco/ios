//
//  EventHeaderView.swift
//  events
//
//  Created by Julio Effgen on 07/07/16.
//  Copyright © 2016 Julio Effgen. All rights reserved.
//

import UIKit

class EventHeaderView: UIView {

	@IBOutlet weak var titleLabel: UILabel!

}
