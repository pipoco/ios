//
//  EventCell.swift
//  events
//
//  Created by Julio Effgen on 07/07/16.
//  Copyright © 2016 Julio Effgen. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

	@IBOutlet weak var eventTitleLabel: UILabel!
	@IBOutlet weak var eventStartLabel: UILabel!
	@IBOutlet weak var eventEndLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		self.preservesSuperviewLayoutMargins = false
		self.separatorInset = UIEdgeInsetsZero
		self.layoutMargins = UIEdgeInsetsZero
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
