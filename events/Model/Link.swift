//
//  Link.swift
//  events
//
//  Created by Julio Effgen on 04/07/16.
//  Copyright © 2016 Julio Effgen. All rights reserved.
//

import Foundation
import CoreData

class Link: NSManagedObject {
	
	// Insert code here to add functionality to your managed object subclass
	@NSManaged var detail: String?
	@NSManaged var link: String?
}