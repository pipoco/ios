//
//  BeginTVC.swift
//  events
//
//  Created by Julio Effgen on 07/07/16.
//  Copyright © 2016 Julio Effgen. All rights reserved.
//

import UIKit

class BeginTVC: UITableViewController {
	
	//MARK: Constants
	
	let kNUMBER_OF_SECTIONS: Int = 40
	let kHEIGHT_OF_SECTION: CGFloat = 25.0
	let kNUMBER_OF_ROWS: Int = 2
	let kEVENT_CELL_ID: String = "EventCell"
	var formatter1 = NSDateFormatter()
	var formatter2 = NSDateFormatter()
	var currentDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -1, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
	
	//MARK: TableView Controller

    override func viewDidLoad() {
        super.viewDidLoad()
		formatter1.dateFormat = "EEE, dd"
		formatter2.dateFormat = "MMMM"
		tableView.registerNib(UINib(nibName: kEVENT_CELL_ID, bundle: nil), forCellReuseIdentifier: kEVENT_CELL_ID)
		self.title = NSLocalizedString("Events", comment: "Title of Event Screen")
    }

	// MARK: TableView Datasource
	
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return kNUMBER_OF_SECTIONS
    }
	
	private func loadHeader() -> EventHeaderView {
		return NSBundle.mainBundle().loadNibNamed("EventHeaderView", owner: nil, options: nil)[0] as! EventHeaderView
	}
	
	override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let titleView = self.loadHeader()
		currentDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 1, toDate: currentDate!, options: NSCalendarOptions(rawValue: 0))
		let dateString = String(format: "%@ %@ %@", formatter1.stringFromDate(currentDate!), NSLocalizedString("of", comment: "Concatenation of"), formatter2.stringFromDate(currentDate!))
		titleView.titleLabel.text = dateString
		return titleView
	}
	
	override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return kHEIGHT_OF_SECTION
	}

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kNUMBER_OF_ROWS
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(kEVENT_CELL_ID, forIndexPath: indexPath) as! EventCell
		
		cell.eventTitleLabel.text = String(format: "%@ -> %@ %@ %@", "Tela Tubis", formatter1.stringFromDate(currentDate!), NSLocalizedString("of", comment: "Concatenation of"), formatter2.stringFromDate(currentDate!))
		return cell
    }
	
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
